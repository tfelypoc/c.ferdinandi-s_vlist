---
title: "Success"
date: 2018-01-11T16:03:33-05:00
draft: false
noTitle: true
---

[&larr; Back to the Vanilla JavaScript Podcast](/)

# Thanks for inviting me into your inbox.

Can't wait to get started? Check out the [Vanilla JS Toolkit](https://vanillajstoolkit.com) to explore a growing collection of code snippets, helper functions, polyfills, plugins, and learning resources.

**I also have a question for you:** what's the biggest challenge you face as a web developer? Send me an email at {{<email>}} and let me know.