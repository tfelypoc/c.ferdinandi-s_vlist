---
title: "Netflix"
date: 2019-02-27T11:46:37-05:00
site: "https://netflix.com"
details: "https://medium.com/dev-channel/a-netflix-web-performance-case-study-c0bcde26a9d9"
draft: false
noTitle: false
noIndex: false
---

By using vanilla JS for their client-side code, *Loading* and *Time-to-Interactive* decreased by 50% (they still use React server-side).