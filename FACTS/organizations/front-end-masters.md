---
title: "Front End Masters"
date: 2019-02-27T11:31:47-05:00
site: "https://frontendmasters.com/"
details: "https://marcgrabanski.com/new-frontend-masters-website"
draft: false
noTitle: false
noIndex: false
---

The app was rebuilt from the ground-up with the goal of being more performant *and* providing a great developer experience.