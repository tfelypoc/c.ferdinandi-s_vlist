---
title: "GitHub"
date: 2019-02-27T11:49:05-05:00
site: "https://github.com/"
details: "https://githubengineering.com/removing-jquery-from-github-frontend/"
draft: false
noTitle: false
noIndex: false
---

GitHub removed jQuery from their app in favor of native browser methods and custom web components.

> Sometimes technical debt grows around dependencies that once provided value, but whose value dropped over time.