---
title: "MeetSpace"
date: 2019-02-27T11:36:01-05:00
site: "https://www.meetspaceapp.com/"
details: "https://www.smashingmagazine.com/2017/05/why-no-framework/"
draft: false
noTitle: false
noIndex: false
---

The average page on MeetSpace is ready in just 200ms.

> Using this approach, we were able to create an incredibly fast and light web application that is also less work to maintain over time.