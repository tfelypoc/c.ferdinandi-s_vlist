---
title: "Vanilla JS List"
date: 2018-01-14T21:38:44-05:00
draft: false
noTitle: false
fullWidth: false
---

<p class="text-large">A growing list of organizations that use vanilla JS to build websites and web apps, curated by <a href="https://gomakethings.com">Chris&nbsp;Ferdinandi</a>.</p>

*__Vanilla JS__ is a term for coding with native JavaScript features and browser APIs instead of frameworks and libraries.*

{{<cta for="vanilla-js-list">}}

<a class="btn btn-large" href="/organizations">Explore the List &rarr;</a>

<div class="list-spaced">
{{%md%}}
{{%/md%}}
</div>

{{<mailchimp intro="true">}}

{{<about-me>}}