---
title: "Learning Resources"
date: 2018-01-24T11:48:20-05:00
draft: false
noTitle: false
noIndex: false
anchors: true
---

Let's make {{<year>}} the year that you master&nbsp;JavaScript! You can do this. I can help.

{{<cta for="learnvjs-resources">}}

<div class="list-spaced">
{{%md%}}
- **[Daily Developer Tips.](/articles/)** I send out a short email each weekday with code snippets, tools, techniques, and interesting stuff from around the web.
- **[Learning Roadmap.](https://learnvanillajs.com)** A vanilla JS roadmap, along with learning resources and project ideas to help you get started.
- **[Podcast.](https://vanillajspodcast.com)** A show about JavaScript for people who hate the complexity of modern front‑end web development.
- **[The Toolkit.](https://vanillajstoolkit.com)** A growing collection of vanilla JavaScript code snippets, helper functions, polyfills, plugins, and learning resources.
- **[Guides & Courses.](https://vanillajsguides.com/)** Short, focused ebooks and video courses made for beginners. Start building real JavaScript projects in under an hour.
- **[Vanilla JS Academy.](https://vanillajsacademy.com/)** A project-based online JavaScript training program for beginners.
{{%/md%}}
</div>